//
//  SongTableViewCell.swift
//  player
//
//  Created by Ruslan Olkhovka on 06.08.17.
//  Copyright © 2017 Ruslan Olkhovka. All rights reserved.
//

import UIKit

class SongTableViewCell: UITableViewCell {
    
    private let keyPath_title = #keyPath(Song.title)
    private let keyPath_artist = #keyPath(Song.artist)
    private let keyPath_progress = #keyPath(Song.progress)
    private let keyPath_artwork = #keyPath(Song.artwork)
    
    var song: Song? {
        didSet {
            updateTitle()
            updateProgress()
            updateImage()
            
            [keyPath_title, keyPath_artist, keyPath_progress, keyPath_artwork].forEach { (keyPath) in
                oldValue?.removeObserver(self, forKeyPath: keyPath)
                song?.addObserver(self, forKeyPath: keyPath, options: .new, context: nil)
            }
        }
    }
    
    override func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey : Any]?, context: UnsafeMutableRawPointer?) {
        switch keyPath! {
        case keyPath_title:
            updateTitle()
        case keyPath_artist, keyPath_progress:
            updateProgress()
        case keyPath_artwork:
            updateImage()
        default:break
        }
    }
    
    func updateTitle() {
        textLabel?.text = song?.title ?? song?.url
    }
    
    func updateProgress() {
        let progress = Int((song?.progress ?? 0) * 100)
        detailTextLabel?.text = song?.artist ?? ((progress > 0) ? "downloading...\(progress)%" : " ")
    }
    
    func updateImage() {
        imageView?.image = song?.artwork
    }
    
    deinit {
        song = nil
    }
}
