//
//  Extensions.swift
//  player
//
//  Created by Ruslan Olkhovka on 04.08.17.
//  Copyright © 2017 Ruslan Olkhovka. All rights reserved.
//

import Foundation
import PromiseKit

extension String {
    var convertingCP1252: String {
        if let chars = cString(using: .windowsCP1252),
            let ret = String(cString: chars, encoding: .windowsCP1251) {
            return ret
        }
        return self
    }
}

extension Promise {
    static func chain(_ promises:[() -> Promise<T>]) -> Promise<[T]> {
        return Promise<[T]> { (fulfill, reject) in
            var out = [T]()
            
            let fp:Promise<T>? = promises.reduce(nil) { (r, o) in
                return r?.then { c in
                    out.append(c)
                    return o()
                    } ?? o()
            }
            
            fp?.then { c -> Void in
                out.append(c)
                fulfill(out)
                }
                .catch(execute: reject)
        }
    }
}

func showAlert(title: String, message: String) -> UIAlertController {
    let alert = UIAlertController(
        title: title,
        message: message,
        preferredStyle: UIAlertControllerStyle.alert
    )
    let ok = UIAlertAction(
        title: "OK",
        style: UIAlertActionStyle.default,
        handler: nil
    )
    alert.addAction(ok)
    UIApplication.shared.keyWindow?.rootViewController?.present(alert, animated: true, completion: nil)
    return alert
}

