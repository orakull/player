//
//  Player.swift
//  player
//
//  Created by Ruslan Olkhovka on 06.08.17.
//  Copyright © 2017 Ruslan Olkhovka. All rights reserved.
//

import UIKit
import AVFoundation
import MediaPlayer
import PromiseKit

protocol PlayerDelegate {
    func playerCurrentSongChanged(_ song: Song?, prevSong: Song?)
    func playerIsPlayingChanged(_ isPlaying: Bool)
}

class Player: NSObject, AVAudioPlayerDelegate {
    
    static let shared: Player = {
        let player = Player()
        UIApplication.shared.beginReceivingRemoteControlEvents()
        let commandCenter = MPRemoteCommandCenter.shared()
        commandCenter.playCommand.addTarget { _ in player.playPause() }
        commandCenter.pauseCommand.addTarget { _ in player.playPause() }
        commandCenter.nextTrackCommand.addTarget { _ in player.play(song: player.nextSong) }
        commandCenter.previousTrackCommand.addTarget { _ in player.play(song: player.prevSong) }
        return player
    }()
    
    private static var audioPlayer: AVAudioPlayer? {
        didSet {
            oldValue?.stop()
            oldValue?.delegate = nil
            audioPlayer?.prepareToPlay()
            audioPlayer?.delegate = shared
        }
    }
    
    static func metadataFor(song: Song) -> Promise<Song> {
        return Promise { fulfill, reject in
            guard let url = song.fileUrl else {
                return reject(DownloaderError.Id3ParseError)
            }
            if song.title != nil, song.artist != nil, song.artist != nil { return fulfill(song) }
            
            let asset = AVAsset(url: url)
            for metadata in asset.commonMetadata {
				if metadata.commonKey!.rawValue == "artwork",
                    let data = metadata.dataValue {
                    song.artwork = UIImage(data: data)
                }
                guard let value = metadata.stringValue?.convertingCP1252 else { continue }
				if metadata.commonKey!.rawValue == "title" {
                    song.title = value
                }
				if metadata.commonKey!.rawValue == "artist" {
                    song.artist = value
                }
            }
            song.save()
            fulfill(song)
        }
    }
    
    var delegate: PlayerDelegate?
    var songs = [Song]()
    var currentSong: Song? {
        didSet {
            delegate?.playerCurrentSongChanged(currentSong, prevSong: oldValue)
        }
    }
    var isPlaying = false {
        didSet {
            delegate?.playerIsPlayingChanged(isPlaying)
        }
    }
    
    var nextSong: Song? {
        if let currentSong = currentSong,
            let index = songs.index(of: currentSong),
            index < songs.count - 1 {
            return songs[index + 1]
        }
        return nil
    }
    
    var prevSong: Song? {
        if let currentSong = currentSong,
            let index = songs.index(of: currentSong),
            index > 0 {
            return songs[index - 1]
        }
        return nil
    }
    
    @discardableResult
    func play(song: Song?) -> MPRemoteCommandHandlerStatus {
        guard song != nil else { return .noSuchContent }
        currentSong = song
        if let url = currentSong?.fileUrl, url != Player.audioPlayer?.url {
            Player.audioPlayer = try? AVAudioPlayer(contentsOf: url)
        }
        return playPause()
    }
    
    func playPause() -> MPRemoteCommandHandlerStatus {
        if Player.audioPlayer?.isPlaying ?? false {
            Player.audioPlayer?.pause()
            isPlaying = false
        } else {
            isPlaying = Player.audioPlayer?.play() ?? false
        }
        update()
        return .success
    }
    
    private func update() {
        if Player.audioPlayer?.isPlaying ?? false {
            var dict = [ String : Any ]()
            if let str = currentSong?.title { dict[MPMediaItemPropertyTitle] = str }
            if let str = currentSong?.artist { dict[MPMediaItemPropertyArtist] = str }
            if let img = currentSong?.artwork {
                dict[MPMediaItemPropertyArtwork] = MPMediaItemArtwork(boundsSize: img.size) { _ in img }
            }
            MPNowPlayingInfoCenter.default().nowPlayingInfo = dict
        }
    }
    
    // MARK: - audio player delegate
    
    func audioPlayerDidFinishPlaying(_ player: AVAudioPlayer, successfully flag: Bool) {
        isPlaying = false
        if flag {
            play(song: nextSong)
        }
    }
}
