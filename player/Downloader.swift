//
//  Downloader.swift
//  player
//
//  Created by Ruslan Olkhovka on 02.08.17.
//  Copyright © 2017 Ruslan Olkhovka. All rights reserved.
//

import Foundation
import Alamofire
import PromiseKit
import AVFoundation

enum DownloaderError: Error {
    case WrongList
    case Id3ParseError
}

protocol DownloaderDelegate {
    func downloaderGetSongs(_ songs: [Song])
}

class Downloader {
    
    static let listUrl = "https://www.dropbox.com/s/8ojpfpz5m3s3reg/list.txt?dl=1"
    
    static var downloadDir: URL? {
        return FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first
    }
    
    var delegate: DownloaderDelegate?
    
    lazy var networkMonitor: Alamofire.NetworkReachabilityManager = {
        let monitor = Alamofire.NetworkReachabilityManager(host: Downloader.listUrl)!
        monitor.startListening()
        return monitor
    }()
    
    func start() {
        self.getList().then { list -> Promise<[Song]> in
            let songs = list.map { Song.GetSong(url: $0) }
            self.delegate?.downloaderGetSongs(songs)
            
            let promises = songs.map { song in
                return {
                    return self.download(song: song)
                        .then(execute: Player.metadataFor)
                }
            }
            
            return Promise.chain(promises)
            }.catch { (error) in
                if !self.networkMonitor.isReachable {
                    let alert = showAlert(title: "Error", message: error.localizedDescription)
                    self.networkMonitor.listener = { status in
                        print(status)
                        if self.networkMonitor.isReachable {
                            alert.dismiss(animated: true)
                            self.networkMonitor.listener = nil
                            self.start()
                        }
                    }
                } else {
                    self.start()
                }
        }
    }
    
    func getList() -> Promise<[String]> {
        return Promise { fulfill, reject in
            let defaults = UserDefaults.standard
            
            if let list = defaults.stringArray(forKey: "list") {
                print("getting local list")
                fulfill(list)
                return
            }
            
            Alamofire.request(Downloader.listUrl).responseString { (response) in
                if let error = response.error {
                    reject(error)
                    return
                }
                if let str = response.value {
                    let list = str.components(separatedBy: CharacterSet.newlines)
                    defaults.set(list, forKey: "list")
                    defaults.synchronize()
                    print("getting server list")
                    fulfill(list)
                } else {
                    reject(DownloaderError.WrongList)
                }
            }
            
        }
    }
    
    func download(song: Song) -> Promise<Song> {
        return Promise { fulfill, reject in
            
            // file already downloaded
            if song.fileName != nil {
                print("alreadey downloaded \(song.url)")
                fulfill(song)
                return
            }
            
            let destination: DownloadRequest.DownloadFileDestination = { tempUrl, response in
                var fileUrl = tempUrl
                if let dirURL = Downloader.downloadDir,
                    let filename = response.suggestedFilename {
                    fileUrl = dirURL.appendingPathComponent(filename)
                }
                return (fileUrl, [.createIntermediateDirectories, .removePreviousFile])
            }
            
            let request: DownloadRequest
            request = Alamofire.download(song.url, to: destination)
            print("start downloading \(song.url)")
            
            request.response { (response) in
                if let error = response.error {
                    reject(error)
                    return
                }
                song.fileName = response.destinationURL!.pathComponents.last!
                song.save()
                fulfill(song)
                print("downloaded: \(song.fileName!)")
                }
                .downloadProgress { song.progress = $0.fractionCompleted }
        }
    }
}
