//
//  Song.swift
//  player
//
//  Created by Ruslan Olkhovka on 05.08.17.
//  Copyright © 2017 Ruslan Olkhovka. All rights reserved.
//

import UIKit

@objc class Song: NSObject {
    var url: String
    var fileName: String?
    @objc dynamic var title: String?
    @objc dynamic var artist: String?
    @objc dynamic var progress = 0.0
    @objc dynamic var artwork: UIImage?
    
    var fileUrl: URL? {
        if let fileName = fileName {
            return Downloader.downloadDir?.appendingPathComponent(fileName)
        }
        return nil
    }
    
    init(_ url: String) {
        self.url = url
        super.init()
        if let dict = UserDefaults.standard.dictionary(forKey: storeKey) {
            fileName = dict["fileName"] as? String
        } else {
            save()
        }
    }
    
    static var songs = [ String : Song ]()
    static func GetSong(url: String) -> Song {
        if let song = songs[url] {
            return song
        }
        let song = Song(url)
        songs[url] = song
        return song
    }
    
    func save() {
        var dict = [String : Any]()
        if let fileName = fileName { dict["fileName"] = fileName }
        UserDefaults.standard.set(dict, forKey: storeKey)
        UserDefaults.standard.synchronize()
    }
    
    private var storeKey: String {
        return "song|".appending(url)
    }
    
}
