//
//  ViewController.swift
//  player
//
//  Created by Ruslan Olkhovka on 01.08.17.
//  Copyright © 2017 Ruslan Olkhovka. All rights reserved.
//

import UIKit

class ViewController: UIViewController, DownloaderDelegate, PlayerDelegate {
    
    let downloader = Downloader()
    var songs = [Song]()
    
    @IBOutlet weak var tableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let nib = UINib(nibName: "SongTableViewCell", bundle: nil)
        tableView.register(nib, forCellReuseIdentifier: "songCell")
        
        downloader.delegate = self
        Player.shared.delegate = self
        
        startLoading()
    }
    
    func startLoading() {
        downloader.start()
        songs.removeAll()
        tableView.reloadData()
        showLoading = true
    }
    
    var showLoading = false {
        didSet {
            let indicator: UIActivityIndicatorView
            if let ind = tableView.backgroundView as? UIActivityIndicatorView {
                indicator = ind
            } else {
                indicator = UIActivityIndicatorView(activityIndicatorStyle: .gray)
                indicator.hidesWhenStopped = true
                indicator.center = tableView.center
                tableView.backgroundView = indicator
            }
            if showLoading {
                indicator.startAnimating()
                tableView.separatorStyle = .none
            } else {
                indicator.stopAnimating()
                tableView.separatorStyle = .singleLine
            }
        }
    }
    
    // MARK: - Downloader delegation
    
    func downloaderGetSongs(_ songs: [Song]) {
        self.songs = songs
        Player.shared.songs = songs
        tableView.reloadData()
        showLoading = false
    }
    
    // MARK: - Player delegation
    
    func playerCurrentSongChanged(_ song: Song?, prevSong: Song? = nil) {
        if let song = song, let index = songs.index(of: song) {
            let indexPath = IndexPath(row: index, section: 0)
            if Player.shared.isPlaying {
                tableView.selectRow(at: indexPath, animated: true, scrollPosition: .middle)
            } else {
                tableView.deselectRow(at: indexPath, animated: true)
            }
        }
        
        if let song = prevSong, let index = songs.index(of: song) {
            tableView.deselectRow(at: IndexPath(row: index, section: 0), animated: true)
        }
    }
    
    func playerIsPlayingChanged(_ isPlaying: Bool) {
        playerCurrentSongChanged(Player.shared.currentSong)
    }
}

extension ViewController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return songs.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "songCell", for: indexPath) as! SongTableViewCell
        cell.song = songs[indexPath.row]
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let song = songs[indexPath.row]
        if song.fileName != nil {
            Player.shared.play(song: song)
        }
    }
    
    func tableView(_ tableView: UITableView, shouldHighlightRowAt indexPath: IndexPath) -> Bool {
        let song = songs[indexPath.row]
        return song.fileName != nil
    }
}
